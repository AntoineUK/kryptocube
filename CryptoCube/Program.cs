﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoCube
{
    class Program
    {
        private static List<Cube> cubeList = new List<Cube>();

        static void Main(string[] args)
        {

            string messageSaisie;

            Console.Write("Saisissez votre message a cypter :\n");
            messageSaisie = Console.ReadLine();

            GenerateCube(messageSaisie);
            ReadCubes();
            DecodeCubes();
            Console.Read();

        }

        private static void DecodeCubes()
        {
            for (int i = 0; i < cubeList.Count(); i++)
            {
                cubeList[i].DecodeCube();
            }
        }

        private static void GenerateCube(string message)
        {
            int nmbOfCube = message.Length / 8;
            int nmbCaracLeft = message.Length % 8;
            Console.Write(nmbOfCube +" cubes et "+ nmbCaracLeft+" lettres \n");
            if (nmbCaracLeft > 0)
            {
                nmbOfCube++;
            }
            string cubeContent;

            for (int i = 1; i <= nmbOfCube; i++)
            {
                Console.WriteLine("\n"+i);

                cubeContent = message;       

                Cube newCube = new Cube();
      
                newCube.Sequence=new List<char>('u');
                newCube.Sequence.Add('u');
                newCube.Sequence.Add('r');
                newCube.Sequence.Add('l');


                if (message.Length >= 8)
                {

                    cubeContent = message.Substring(0, 8);
                    newCube.write(cubeContent);
                    message = message.Remove(0,8);

                }
                else
                {
                    newCube.write(cubeContent);
                }

                cubeList.Add(newCube);
                PrintCube(newCube.ReadCube());
            }

        }

        private static void PrintCube(string messageCube)
        {
            if (messageCube.Length < 8)
            {
                messageCube = filler(messageCube);
            }
            Console.Write("            "+messageCube[0]+ "____" + messageCube[1] + "\n           /|   /|\n          / " + messageCube[2] + "__/_" + messageCube[3] + "\n         " + messageCube[4] + " _/_" + messageCube[5] + "  /\n         | /   |/\n         " + messageCube[6] + "/____" + messageCube[7] + "");
        }



        public static string filler(string s)
        {
            int taille = s.Length;
            int reste = 8 - taille;
            string serie = new String('.', reste);
            string stringToReturn = s+serie;
            return stringToReturn;
        }


        private static void ReadCubes()
        {
            string stringToWrite = "\n";

            for (int i = 0; i < cubeList.Count; i++)
            {
                stringToWrite = stringToWrite + cubeList[i].ReadCube();

            }
            Console.WriteLine(stringToWrite);
        }
    }
}
